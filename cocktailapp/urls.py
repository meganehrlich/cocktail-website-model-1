from django.contrib import admin
from django.urls import include, path
from .views import list_cocktails, create_cocktail
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('create/', create_cocktail, name="create_cocktail"),
    path('', list_cocktails, name="list_cocktails"),
]
