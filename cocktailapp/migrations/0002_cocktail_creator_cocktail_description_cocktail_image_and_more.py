# Generated by Django 4.0.6 on 2022-07-22 00:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cocktailapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cocktail',
            name='creator',
            field=models.CharField(blank=True, max_length=120, null=True),
        ),
        migrations.AddField(
            model_name='cocktail',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='cocktail',
            name='image',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='cocktail',
            name='liquor',
            field=models.ManyToManyField(blank=True, null=True, related_name='cocktail', to='cocktailapp.liquor'),
        ),
    ]
