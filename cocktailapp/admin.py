from django.contrib import admin
from cocktailapp.models import Cocktail, Liquor

# Register your models here.

admin.site.register(Cocktail)
admin.site.register(Liquor)