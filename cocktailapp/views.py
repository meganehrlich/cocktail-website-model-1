from multiprocessing import context
from django.shortcuts import render, redirect, get_object_or_404
from .models import Cocktail, Liquor
from .forms import CocktailForm

# Create your views here.
def list_cocktails(request):
    cocktails = Cocktail.objects.all()
    context = {
        "cocktails": cocktails
    }
    return render(request, "cocktails/list.html", context)

def create_cocktail(request):
    context = {}
    form = CocktailForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_cocktails")
        
    context["form"]=form
    return render(request, "cocktails/create.html", context)